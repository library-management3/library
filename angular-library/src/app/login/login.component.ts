import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: any;
  hide = true;
  user;

  constructor(
    public auth: AuthService,
    private toastr: ToastrService,
    public router: Router
  ) {
    this.loginForm = FormGroup;
    console.log(this.auth.getUser())
    this.user = JSON.parse(this.auth.getUser());

    if (this.user && this.user.isLoggedIn) {
      let url = this.user.userType == 'admin' ? '/admin' : 'student'
      this.router.navigate([url])
    }
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(4)]),
      userType: new FormControl('admin', [Validators.required])
    });
  }

  getErrorMessage(val: string) {
    if (val == 'email') {
      if (this.loginForm.get("email").hasError('required')) {
        return 'You must enter a value';
      }

      return this.loginForm.get("email").hasError('email') ? 'Not a valid email' : '';
    } else {
      if (this.loginForm.get("password").hasError('required')) {
        return 'You must enter a value';
      }

      return this.loginForm.get("password").hasError('email') ? 'Please enter valid password' : '';
    }
  }

  onSubmit(): void {
    this.login(this.loginForm.value)
  }

  login(data: any) {
    const { userType, email, password } = data;

    if (userType == "admin") {
      this.auth.adminLogin({
        email, password
      }).subscribe(async (res: any) => {
        if (!res) this.toastr.error(`Email or usertype or password is incorrect or wrong`);
        else {
          let userInfo = await this.auth.getAdmin(email)
          this.auth.storeLoggedInUser({ ...userInfo,userType, isLoggedIn: res });
          this.toastr.success(`Admin ${email} logged in successfully`);
          this.router.navigate(['/admin'])
        }
      }, (error: HttpErrorResponse) => {
        this.toastr.error(error.statusText);
      })
    } else {
      this.auth.studentLogin({
        email, password
      }).subscribe(async (res: any) => {
        if (!res) this.toastr.error(`Email or usertype or password is incorrect or wrong`);
        else {
          let userInfo = await this.auth.getUserByEmail(email)
          this.auth.storeLoggedInUser({ ...userInfo, userType, isLoggedIn: res })
          this.toastr.success(`Student ${email} logged in successfully`);
          this.router.navigate(['/student'])
        }
      }, (error: HttpErrorResponse) => {
        this.toastr.error(error.statusText);
      })
    }
  }
}
