import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { BooksService } from '../services/books.service';

@Component({
  selector: 'app-student-dashboard',
  templateUrl: './student-dashboard.component.html',
  styleUrls: ['./student-dashboard.component.css']
})
export class StudentDashboardComponent implements OnInit {
  userInfo: any;
  unreturn: any;

  constructor(
    private router: Router,
    private auth: AuthService,
    private book: BooksService
  ) { }

  ngOnInit(): void {
    this.getUserInfo()
  }

  logout() {
    this.auth.logout()
    this.router.navigate(['/login'])
  }

  getUserInfo() {
    this.userInfo = JSON.parse(this.auth.getUser())
    this.getUnreturnedData(this.userInfo.studentId)
  }

  async getUnreturnedData(id: any) {
    this.unreturn = await this.book.getUnreturnedStudentInfo(id)
  }

}
