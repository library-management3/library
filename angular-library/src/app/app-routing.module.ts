import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { StudentDashboardComponent } from './student-dashboard/student-dashboard.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { UserComponent } from './user/user/user.component';
import { StudentSearchComponent } from './user/student-search/student-search.component';
import { UBookComponent } from './unreturned/book/book.component';
import { UStudentComponent } from './unreturned/student/student.component';
import { BookSearchComponent } from './books/book-search/book-search.component';
import { BookReturnComponent } from './books/book-return/book-return.component';
import { BookEntryComponent } from './books/book-entry/book-entry.component';
import { BookComponent } from './books/book/book.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'student', component: StudentDashboardComponent },
  { 
    path: 'admin', 
    component: AdminDashboardComponent
  },
  {
    path: 'admin/add',
    component: UserComponent
  },
  {
    path: 'admin/search',
    component: StudentSearchComponent
  },
  {
    path: 'unreturned/book',
    component: UBookComponent
  },
  {
    path: 'unreturned/student',
    component: UStudentComponent
  },
  {
    path: 'book',
    component: BookComponent
  },
  {
    path: 'book/entry',
    component: BookEntryComponent
  },
  {
    path: 'book/return',
    component: BookReturnComponent
  },
  {
    path: 'book/search',
    component: BookSearchComponent
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
