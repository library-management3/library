import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UBookComponent } from './book.component';

describe('UBookComponent', () => {
  let component: UBookComponent;
  let fixture: ComponentFixture<UBookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UBookComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
