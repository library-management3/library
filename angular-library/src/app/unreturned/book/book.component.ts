import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class UBookComponent implements OnInit {
  unreturnForm: any = FormGroup;
  unreturnedBook: any = null;

  constructor(
    private toastr: ToastrService,
    private users: UserService
  ) {
    this.unreturnForm = FormGroup;
  }

  ngOnInit(): void {
    this.unreturnForm = new FormGroup({
      bookId: new FormControl('', [Validators.required])
    });
  }

  getErrorMessage(val: string) {

    if (this.unreturnForm.get("bookId").hasError('required')) {
      return 'You must enter a value';
    }

    return this.unreturnForm.get("bookId").hasError('bookId') ? 'Please enter valid bookId' : '';

  }

  onSubmit(): void {
    this.unreturnBook(this.unreturnForm.value.bookId)
  }

  unreturnBook(id: string) {
    this.users.unreturnedBook(id).then((res) => {
      this.unreturnedBook = res;
    }).catch((err) => {
      this.toastr.error(err.error.message);
    })
  }
}
