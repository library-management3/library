import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class UStudentComponent implements OnInit {
  unreturnForm: any = FormGroup;
  unreturnedStudent: any = null;
  constructor(
    private toastr: ToastrService,
    private users: UserService
  ) {
    this.unreturnForm = FormGroup;
  }

  ngOnInit(): void {
    this.unreturnForm = new FormGroup({
      studentId: new FormControl('', [Validators.required])
    });
  }

  getErrorMessage(val: string) {

    if (this.unreturnForm.get("studentId").hasError('required')) {
      return 'You must enter a value';
    }

    return this.unreturnForm.get("studentId").hasError('studentId') ? 'Please enter valid studentId' : '';

  }

  onSubmit(): void {
    this.unreturnStudent(this.unreturnForm.value.studentId)
  }

  unreturnStudent(id: string) {
    this.users.unreturnedStudent(id).then((res) => {
      this.unreturnedStudent = res;
    }).catch((err) => {
      this.toastr.error(err.error.message);
    })
  }
}
