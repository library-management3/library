import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UStudentComponent } from './student.component';

describe('UStudentComponent', () => {
  let component: UStudentComponent;
  let fixture: ComponentFixture<UStudentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UStudentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
