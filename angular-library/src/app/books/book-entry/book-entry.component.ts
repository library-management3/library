import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-book-entry',
  templateUrl: './book-entry.component.html',
  styleUrls: ['./book-entry.component.css']
})
export class BookEntryComponent implements OnInit {
  entryForm: any = FormGroup;

  constructor(
    private toastr: ToastrService,
    private book: BooksService
  ) {
    this.entryForm = FormGroup;
  }

  ngOnInit(): void {
    this.entryForm = new FormGroup({
      bookId: new FormControl('', [Validators.required]),
      studentId: new FormControl('', [Validators.required]),
    });
  }

  getErrorMessage(val: string) {
    if (val == 'bookId') {
      if (this.entryForm.get("bookId").hasError('required')) {
        return 'You must enter a value';
      }

      return this.entryForm.get("bookId").hasError('bookId') ? 'Not a valid bookId' : '';
    } else {
      if (this.entryForm.get("studentId").hasError('required')) {
        return 'You must enter a value';
      }

      return this.entryForm.get("studentId").hasError('studentId') ? 'Please enter valid studentId' : '';
    }
  }

  onSubmit(): void {
    this.addEntry(this.entryForm.value)
  }

  addEntry(data: any) {
    this.book.addEntry(data).subscribe((res: any) => {
      this.toastr.success(`Book entry added  successfully`);
      this.entryForm.reset(this.entryForm.value);
    }, (error: HttpErrorResponse) => {
      this.toastr.error(error.error.message);
    })
  }
}
