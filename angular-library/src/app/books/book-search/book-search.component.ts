import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BooksService } from 'src/app/services/books.service';


@Component({
  selector: 'app-book-search',
  templateUrl: './book-search.component.html',
  styleUrls: ['./book-search.component.css']
})
export class BookSearchComponent implements OnInit {
  searchForm: any = FormGroup;
  bookDetail: any = null

  constructor(
    private toastr: ToastrService,
    private books: BooksService
  ) {
    this.searchForm = FormGroup;
  }

  ngOnInit(): void {
    this.searchForm = new FormGroup({
      bookId: new FormControl('', [Validators.required])
    });
  }

  getErrorMessage(val: string) {

    if (this.searchForm.get("bookId").hasError('required')) {
      return 'You must enter a value';
    }

    return this.searchForm.get("bookId").hasError('bookId') ? 'Please enter valid bookId' : '';

  }

  onSubmit(): void {
    this.searchBook(this.searchForm.value.bookId)
  }

  searchBook(id: any) {
    this.books.searchBookId(id).subscribe((res: any) => {
      this.bookDetail = res;
    }, (error: HttpErrorResponse) => {
      this.toastr.error(error.error.message);
    })
  }


}
