import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Inject } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BooksService } from 'src/app/services/books.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})

export class BookComponent implements OnInit {
  bookList: any = [];
  mdTitle: string = 'Add Book'

  constructor(
    private books: BooksService,
    public toastr: ToastrService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getBooks()
  };

  openDialog(enterAnimationDuration: string, exitAnimationDuration: string): void {
    this.dialog.open(AddBookModel, {
      width: '35vw',
      enterAnimationDuration,
      exitAnimationDuration,
      data: {
        mdTitle: this.mdTitle
      }
    });
  }

  addBook() {

  }

  getBooks(): void {
    this.books.getBooks().subscribe((res: any) => {
      this.bookList = res;
    }, (error: HttpErrorResponse) => {
      this.toastr.error(error.statusText);
    })
  }

}

@Component({
  selector: 'add-book-modal',
  templateUrl: './add-book.html',
})
export class AddBookModel {
  dialogTitle: any;
  bookForm: any = FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddBookModel>,
    private books: BooksService,
    public toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.bookForm = FormGroup;
  }

  ngOnInit() {
    this.dialogTitle = this.data.mdTitle;
    this.bookForm = new FormGroup({
      bookName: new FormControl('', [Validators.required]),
      author: new FormControl('', [Validators.required]),
      edition: new FormControl('', [Validators.required, Validators.min(1)]),
      publishedDate: new FormControl('', [Validators.required]),
      pages: new FormControl('', [Validators.required, Validators.min(1)]),
      totalCopies: new FormControl('', [Validators.required, Validators.min(1)]),
      availableCopies: new FormControl('', [Validators.required, Validators.min(1)]),
    });
  }

  getErrorMessage(val: string) {
    if (val == 'bookName') {
      if (this.bookForm.get("bookName").hasError('required')) {
        return 'You must enter a value';
      } else {
        return this.bookForm.get("bookName").hasError('bookName') ? 'Not a valid name' : '';
      }
    } else if (val == 'author') {
      if (this.bookForm.get("author").hasError('required')) {
        return 'You must enter a value';
      } else {
        return this.bookForm.get("author").hasError('author') ? 'Not a valid author' : '';
      }
    } else if (val == 'edition') {
      if (this.bookForm.get("edition").hasError('required')) {
        return 'You must enter a value';
      } else {
        return this.bookForm.get("edition").hasError('edition') ? 'Not a valid editor' : '';
      }
    } else if (val == 'publishedDate') {
      if (this.bookForm.get("publishedDate").hasError('required')) {
        return 'You must enter a value';
      } else {
        return this.bookForm.get("publishedDate").hasError('publishedDate') ? 'Not a valid date' : '';
      }
    } else if (val == 'pages') {
      if (this.bookForm.get("pages").hasError('required')) {
        return 'You must enter a value';
      } else {
        return this.bookForm.get("pages").hasError('pages') ? 'Not a valid page' : '';
      }
    } else if (val == 'totalCopies') {
      if (this.bookForm.get("totalCopies").hasError('required')) {
        return 'You must enter a value';
      } else {
        return this.bookForm.get("totalCopies").hasError('totalCopies') ? 'Not a valid number' : '';
      }
    } else if (val == 'availableCopies') {
      if (this.bookForm.get("availableCopies").hasError('required')) {
        return 'You must enter a value';
      } else {
        return this.bookForm.get("availableCopies").hasError('availableCopies') ? 'Not a valid number' : '';
      }
    }
    else return
  }

  onSubmit(): void {
    console.log(this.bookForm.value.publishedDate, "this.bookForm.value.publishedDate")
    this.bookForm.value.publishedDate = new Date(this.bookForm.value.publishedDate)
    this.addBook(this.bookForm.value)
  }

  addBook(data: any) {
    this.books.addBook(data).subscribe((res: any) => {
      this.toastr.success(`Book added successfully`);
      this.dialogRef.close();
    }, (error: HttpErrorResponse) => {
      this.toastr.error(error.statusText);
    })

  }
}