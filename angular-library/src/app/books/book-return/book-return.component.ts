import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Inject } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BooksService } from 'src/app/services/books.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-book-return',
  templateUrl: './book-return.component.html',
  styleUrls: ['./book-return.component.css']
})
export class BookReturnComponent implements OnInit {
  returnForm: any = FormGroup;

  constructor(
    private books: BooksService,
    public toastr: ToastrService,
    public dialog: MatDialog
  ) {
    this.returnForm = FormGroup;
  }

  ngOnInit(): void {
    this.returnForm = new FormGroup({
      bookId: new FormControl('', [Validators.required]),
      studentId: new FormControl('', [Validators.required]),
    });
  }

  getErrorMessage(val: string) {
    if (val == 'bookId') {
      if (this.returnForm.get("bookId").hasError('required')) {
        return 'You must enter a value';
      }

      return this.returnForm.get("bookId").hasError('bookId') ? 'Not a valid bookId' : '';
    } else {
      if (this.returnForm.get("studentId").hasError('required')) {
        return 'You must enter a value';
      }

      return this.returnForm.get("studentId").hasError('studentId') ? 'Please enter valid studentId' : '';
    }
  }

  async openDialog(enterAnimationDuration: string, exitAnimationDuration: string) {
    let checkFine;
    this.books.checkFine(this.returnForm.value)
      .then((res) => {
        console.log(res)
        checkFine = res;
        this.dialog.open(CheckFineModel, {
          width: '35vw',
          enterAnimationDuration,
          exitAnimationDuration,
          data: {
            ...this.returnForm.value,
            checkFine
          }
        });
      })
      .catch((err) => {
        return this.toastr.error(err.error.message);
      });


  }
}

@Component({
  selector: 'check-fine-modal',
  templateUrl: './check_fine_modal.html',
})
export class CheckFineModel {
  dialogTitle: any = 'Validating Fine';
  checkFine: any;
  bookId: any;
  studentId: any;

  constructor(
    public dialogRef: MatDialogRef<CheckFineModel>,
    private books: BooksService,
    public toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log(this.data)
    this.checkFine = this.data.checkFine;
    this.bookId = this.data.bookId;
    this.studentId = this.data.studentId;
  }

  submitReturn(): void {
    let data = this.checkFine

    this.books.addReturn(data).subscribe((res: any) => {
      this.toastr.success(`Book returned successfully`);
      this.dialogRef.close();
    }, (error: HttpErrorResponse) => {
      console.log(error)
      this.toastr.error(error.error.message);
    })
  }
}