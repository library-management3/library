import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatCardModule } from '@angular/material/card'
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio'

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { StudentDashboardComponent } from './student-dashboard/student-dashboard.component';
import { HttpClientModule } from '@angular/common/http';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HeaderComponent } from './header/header.component';
import { BookEntryComponent } from './books/book-entry/book-entry.component';
import { BookReturnComponent, CheckFineModel } from './books/book-return/book-return.component';
import { BookSearchComponent } from './books/book-search/book-search.component';
import { AddBookModel, BookComponent } from './books/book/book.component';
import { UBookComponent } from './unreturned/book/book.component';
import { UStudentComponent } from './unreturned/student/student.component';
import { UserComponent } from './user/user/user.component';
import { StudentSearchComponent } from './user/student-search/student-search.component';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AdminDashboardComponent,
    StudentDashboardComponent,
    HeaderComponent,
    BookEntryComponent,
    BookReturnComponent,
    BookSearchComponent,
    BookComponent,
    UBookComponent,
    UStudentComponent,
    UserComponent,
    StudentSearchComponent,
    AddBookModel,
    CheckFineModel
  ],
  imports: [
    // BrowserAnimationsModule,
    ToastrModule.forRoot(),
    BrowserModule,
    NoopAnimationsModule,
    AppRoutingModule,
    MatCardModule,
    MatInputModule,
    MatRadioModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }
