import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-student-search',
  templateUrl: './student-search.component.html',
  styleUrls: ['./student-search.component.css']
})
export class StudentSearchComponent implements OnInit {
  searchForm: any = FormGroup;
  studentDetail: any = null
  constructor(
    private toastr: ToastrService,
    private users: UserService
  ) {
    this.searchForm = FormGroup;
  }

  ngOnInit(): void {
    this.searchForm = new FormGroup({
      studentId: new FormControl('', [Validators.required])
    });
  }

  getErrorMessage(val: string) {

    if (this.searchForm.get("studentId").hasError('required')) {
      return 'You must enter a value';
    }

    return this.searchForm.get("studentId").hasError('studentId') ? 'Please enter valid studentId' : '';

  }

  onSubmit(): void {
    this.searchUser(this.searchForm.value.studentId)
  }

  searchUser(id: any) {
    this.users.searchUserbyId(id).subscribe((res: any) => {
      this.studentDetail = res;
    }, (error: HttpErrorResponse) => {
      this.toastr.error(error.error.message);
    })
  }


}
