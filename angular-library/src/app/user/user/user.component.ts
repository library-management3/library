import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  adminForm: any;
  studentForm: any;

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private users: UserService
  ) {
    this.adminForm = FormGroup;
    this.studentForm = FormGroup;
  }

  ngOnInit(): void {
    this.adminForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      adminName: new FormControl('', [Validators.required]),
      dob: new FormControl('', [Validators.required, Validators.email]),
      address: new FormControl('', [Validators.required]),
      phonenumber: new FormControl('', [Validators.required])
    });
    this.studentForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      studentName: new FormControl('', [Validators.required]),
      dob: new FormControl('', [Validators.required, Validators.email]),
      department: new FormControl('', [Validators.required]),
      yearOfJoin: new FormControl('', [Validators.required]),
      yearOfPass: new FormControl('', [Validators.required]),
      phonenumber: new FormControl('', [Validators.required])
    });
  }

  getAdminErrorMessage(val: string) {
    if (this.adminForm.get("email").hasError('required')) {
      return 'You must enter a value';
    }

    return this.adminForm.get("email").hasError('email') ? 'Not a valid email' : '';
  }

  getStudentErrorMessage(val: string) {
    if (this.studentForm.get("email").hasError('required')) {
      return 'You must enter a value';
    }

    return this.studentForm.get("email").hasError('email') ? 'Not a valid email' : '';
  }

  adminSubmit(): void {
    this.adminAdd(this.adminForm.value)
  }

  studentSubmit(): void {
    this.studentAdd(this.adminForm.value)
  }

  adminAdd(data: any) {
    this.users.addAdmin(data).subscribe((res: any) => {
      this.toastr.success(`Admin added  successfully`);
      this.adminForm.reset(this.adminForm.value);
    }, (error: HttpErrorResponse) => {
      this.toastr.error(error.error.message);
    })
  }

  studentAdd(data: any) {
    this.users.addUser(data).subscribe((res: any) => {
      this.toastr.success(`Student added  successfully`);
      this.studentForm.reset(this.studentForm.value);
    }, (error: HttpErrorResponse) => {
      this.toastr.error(error.error.message);
    })
  }

}
