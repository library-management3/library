import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl: string = environment.serverUrl;
  httpOptions = {
    headers: new HttpHeaders({
      Accept: "application/json",
      "Content-Type": "application/json",
    }),
  };

  constructor(
    private http: HttpClient,
  ) { }

  getUsers(): Observable<any> {
    return this.http
      .get(
        `http://localhost:9100/students`,
        this.httpOptions
      )
      .pipe(map((data) => data));
  };

  addUser(data: any): Observable<any> {
    return this.http
      .post(
        `http://localhost:9100/student/add`, data,
        this.httpOptions
      )
      .pipe(map((data) => data));
  }

  addAdmin(data: any): Observable<any> {
    return this.http
      .post(
        `http://localhost:9300/admin/add`, data,
        this.httpOptions
      )
      .pipe(map((data) => data));
  }

  searchUserbyId(id: any): Observable<any> {
    return this.http.get(
      `http://localhost:9100/student/` + id,
      this.httpOptions
    ).pipe(map((data) => data));
  }

  unreturnedBook(id: any): Promise<any> {
    return this.http.get(
      `http://localhost:9400/unreturned/book/` + id,
      this.httpOptions
    ).toPromise();
  }

  unreturnedStudent(id: any): Promise<any> {
    return this.http.get(
      `http://localhost:9400/unreturned/student/` + id,
      this.httpOptions
    ).toPromise();
  }
}
