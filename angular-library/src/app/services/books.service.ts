import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  baseUrl: string = environment.serverUrl;
  httpOptions = {
    headers: new HttpHeaders({
      Accept: "application/json",
      "Content-Type": "application/json",
    }),
  };

  constructor(
    private http: HttpClient,
  ) { }

  getBooks(): Observable<any> {
    return this.http
      .get(
        `http://localhost:9200/books`,
        this.httpOptions
      )
      .pipe(map((data) => data));
  };

  addBook(data: any): Observable<any> {
    return this.http
      .post(
        `http://localhost:9200/book/add`, data,
        this.httpOptions
      )
      .pipe(map((data) => data));
  }

  getUnreturnedStudentInfo(id: any): Promise<any> {
    return this.http.get(
      `http://localhost:9400/unreturned/student/` + id,
        this.httpOptions
    ).toPromise()
  }

  addEntry(data: any): Observable<any> {
    return this.http
      .post(
        `http://localhost:9400/entry`, data,
        this.httpOptions
      )
      .pipe(map((data) => data));
  }

  addReturn(data: any): Observable<any> {
    return this.http
    .post(
      `http://localhost:9400/return`, data,
      this.httpOptions
    )
    .pipe(map((data) => data));
  }

  checkFine(data: any): Promise<any> {
    return this.http
    .post(
      `http://localhost:9400/calculatefine`, data,
      this.httpOptions
    )
    .toPromise();
  }

  searchBookId(id: any): Observable<any> {
    return this.http.get(
      `http://localhost:9200/book/` + id,
        this.httpOptions
    ).pipe(map((data) => data));
  }
}
