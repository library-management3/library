import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "../../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl: string = environment.serverUrl;
  httpOptions = {
    headers: new HttpHeaders({
      Accept: "application/json",
      "Content-Type": "application/json",
    }),
  };
  
  constructor(
    private http: HttpClient,
  ) { }

  adminLogin(data: any): Observable<any> {
    return this.http
      .post(
        `http://localhost:9300/admin/login`, data,
        this.httpOptions
      )
      .pipe(map((data) => data));
  }

  studentLogin(data: any): Observable<any> {
    return this.http
      .post(
        `http://localhost:9100/student/login`, data,
        this.httpOptions
      )
      .pipe(map((data) => data));
  }

  storeLoggedInUser(data: any) {
    return localStorage.setItem('userInfo', JSON.stringify(data))
  }

  getUser(): any {
    return localStorage.getItem('userInfo')
  }

  logout(): void {
    return localStorage.clear()
  }

  getAdmin(email: string): Promise<any> {
    return this.http
      .get(
        `http://localhost:9300/admin/email/` + email,
        this.httpOptions
      )
      .toPromise();
  }

  getUserByEmail(email: string): Promise<any> {
    return this.http
      .get(
        `http://localhost:9100/student/email/` + email,
        this.httpOptions
      )
      .toPromise();
  }
}
