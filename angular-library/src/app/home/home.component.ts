import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { BooksService } from '../services/books.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  bookList: any = []
  constructor(
    public router: Router,
    private auth: AuthService,
    private books: BooksService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.getBooks()
  }

  goToLoginPage() {
    this.router.navigate(['/login'])
  }

  getBooks(): void {
    this.books.getBooks().subscribe((res: any) => {
      this.bookList = res;
    }, (error: HttpErrorResponse) => {
      this.toastr.error(error.statusText);
    })
  }

}
