insert into access_book (id, book_id, student_id, issue_date, due_date, active_status) values (800001, 20001, 10001, '2022-08-02', '2022-08-17', true);
insert into access_book (id, book_id, student_id, issue_date, due_date, active_status) values (800002, 20002, 10001, '2022-07-31', '2022-08-15', true);
insert into access_book (id, book_id, student_id, issue_date, due_date, active_status) values (800003, 20003, 10001, '2022-07-26', '2022-08-10', true);
insert into access_book (id, book_id, student_id, issue_date, due_date, active_status) values (800004, 20001, 10002, '2022-08-04', '2022-08-19', true);
insert into access_book (id, book_id, student_id, issue_date, due_date, active_status) values (800005, 20004, 10002, '2022-07-28', '2022-08-12', true);
insert into access_book (id, book_id, student_id, issue_date, due_date, active_status) values (800006, 20005, 10003, '2022-08-12', '2022-08-27', true);
